Updated: Jan 2015

==How to use this guide==
This guide contains suggested resources that may or may not assist you in the creation of a custom DevPack plug-in module that is compatible with Texas Instruments SensorTag kits. This guide is meant to be helpful to novice hardware designers but may also assist more experienced designers. 

This is a compilation of design resources to get someone started on designing and eventually building their own DevPack module for Texas Instruments SensorTags. This is a good place to get started, but not an exhaustive list. Designing your hardware is solely your responsibility and proper research into the available tools and resources is always a good idea. This list only acts as a suggestion guide. 

The template available here can help give you a jumpstart with your DevPack design process! 

==== A quick introduction to the featured design tools: ==== 

Currently templates are only available for Cadence, more will come.

Cadence - http://www.altium.com/en/products/evaluate
This is a professional design tool that offers many more advanced hardware design features



