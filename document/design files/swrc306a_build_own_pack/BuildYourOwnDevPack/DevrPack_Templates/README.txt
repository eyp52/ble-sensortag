This is a compilation of design resources to get someone started on designing and eventually building their own DevPack module for Texas Instruments SensorTags. This is a good place to get started, but not an exhaustive list. Designing your hardware is solely your responsibility and proper research into the available tools and resources is always a good idea. This list only acts as a suggestion guide. 

The template available here can help give you a jumpstart with your DevPack design process! 

==== A quick introduction to the featured design tools: ==== 

Currently templates are only available for Cadence, more will come.

Cadence is a popular industry rated hardware design tool.  Learn more at www.cadence.com




